Name:           pnm2ppa
Version:        1.13
Release:        1
Summary:        HP PPA GhostScript filter
License:        GPLv2+ and GPLv2
URL:            https://sourceforge.net/projects/pnm2ppa    
Source0:        https://download.sourceforge.net/pnm2ppa/pnm2ppa-%{version}.tar.gz
Source1:        http://www.httptech.com/ppa/files/ppa-0.8.6.tar.gz

Patch1:         pbm2ppa-20000205.diff
Patch2:         pnm2ppa-coverity-return-local.patch
Patch3:         pnm2ppa-gcc10.patch
Patch9000:      bugfix-debranding.patch

BuildRequires:  gcc autoconf automake

%description
pnm2ppa is a Ghostscript print filter which allows owners of HP 
DeskJet 710C, 712C, 720C, 722C, 820Cse, 820Cxi, 1000Cse, or 
1000Cxi printers to print PostScript Level 2.

%package_help

%prep
%setup -q
%autosetup -D -a 1 -p1

autoreconf -vfi

%build
%set_build_flags
%configure
%make_build

pushd pbm2ppa-0.8.6
%make_build
popd

%install
install -d -m 0755 %{buildroot}%{_bindir}
install -d -m 0755 %{buildroot}%{_sysconfdir} 
install -d -m 0755 %{buildroot}%{_mandir}/man1
%make_install INSTALLDIR=%{buildroot}%{_bindir} CONFDIR=%{buildroot}%{_sysconfdir} \
    MANDIR=%{buildroot}%{_mandir}/man1

pushd utils/Linux
install -p -m 0755 detect_ppa test_ppa %{buildroot}%{_bindir}
popd

pushd pbm2ppa-0.8.6
install -p -m 0755 pbm2ppa pbmtpg %{buildroot}%{_bindir}
install -p -m 0644 pbm2ppa.conf %{buildroot}%{_sysconfdir}
install -p -m 0644 pbm2ppa.1 %{buildroot}%{_mandir}/man1
popd

chmod 644 docs/en/LICENSE

%files
%defattr(-,root,root)
%license docs/en/LICENSE
%config(noreplace) %{_sysconfdir}/pnm2ppa.conf
%config(noreplace) %{_sysconfdir}/pbm2ppa.conf
%{_bindir}/*

%files          help
%doc docs/en/RELEASE-NOTES
%doc README Changelog docs/en/INSTALL* docs/en/CREDITS
%doc docs/en/COLOR.* docs/en/CALIBRATION.* docs/en/TODO
%{_mandir}/man1/*

%changelog
* Thu Mar 21 2024 xu_ping<707078654@qq.com> - 1.13-1
- Upgrade version to 1.13

* Fri Jul 30 2021 wangyong<wangyong187@huawei.com> - 1.04-43
- Patch for GCC-10

* Mon Feb 17 2020 wanjiankang<wanjiankang@huawei.com> - 1.04-42
- Package init

